import { Ref, ref, watch } from "vue";
import { CursorDetectionHandler, useDetectClick } from "./useDetectClick";
import { useDragging } from "./useDragging";

export interface Area {
	top: number;
	right: number;
	bottom: number;
	left: number;
}

export interface Pos {
	x: number;
	y: number;
}

export function useMovable(
	elemRef: Ref<HTMLElement | undefined>,
	initialPos: Ref<Pos | undefined>,
	handleCursorDetection?: CursorDetectionHandler
): Ref<Pos | undefined> {
	const movingTo = ref();
	const currentlyPressed = useDetectClick(elemRef, handleCursorDetection);
	const movedTo = useDragging(elemRef, currentlyPressed, (offsetX, offsetY) => {
		if (!elemRef.value) return;
		const move = {
			x: elemRef.value.offsetLeft + offsetX,
			y: elemRef.value.offsetTop + offsetY,
		};
		movingTo.value = move;
		return move;
	});

	watch(initialPos, () => {
		movingTo.value = initialPos.value;
	});

	watch(movingTo, () => {
		if (!elemRef.value || !movingTo.value) return;
		elemRef.value.style.top = `${movingTo.value.y}px`;
		elemRef.value.style.left = `${movingTo.value.x}px`;
	});

	return movedTo;
}
