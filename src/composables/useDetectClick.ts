import { Ref, onMounted, onUnmounted, ref } from "vue";

export type CursorDetectionHandler = (
	rect: DOMRect,
	mouse: MouseEvent
) => boolean;

export function useDetectClick(
	elemRef: Ref<HTMLElement | undefined>,
	handleCursorDetection: CursorDetectionHandler = (rect, mouse) => {
		return (
			rect.left < mouse.clientX &&
			rect.right > mouse.clientX &&
			rect.top < mouse.clientY &&
			rect.bottom > mouse.clientY
		);
	}
): Ref<boolean> {
	const currentlyPressed = ref(false);

	const mousedownCb = (e: MouseEvent) => {
		if (!elemRef.value) return;
		const rect = elemRef.value.getBoundingClientRect();
		currentlyPressed.value =
			handleCursorDetection(rect, e) &&
			document.elementsFromPoint(e.clientX, e.clientY)[0] === elemRef.value;
	};

	onMounted(() => {
		window.addEventListener("mousedown", mousedownCb);
	});

	onUnmounted(() => {
		window.removeEventListener("mousedown", mousedownCb);
	});

	return currentlyPressed;
}
