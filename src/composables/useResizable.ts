import { Ref, onMounted, onUnmounted, ref, watch } from "vue";
import { Pos } from "./useMovable";
import { useDetectClick } from "./useDetectClick";
import { useDragging } from "./useDragging";

type Edge = "top" | "right" | "bottom" | "left";

export interface Size {
	w: number;
	h: number;
}

export function useResizable(
	elemRef: Ref<HTMLElement | undefined>,
	initialSize: Ref<Size | undefined>,
	handleEdgeDetection: (rect: DOMRect, mouse: MouseEvent) => Edge | undefined
): Ref<(Pos & Size) | undefined> {
	const activeEdge = ref<Edge>();
	const movingToSize = ref<Size>();
	const movingToPos = ref<Pos>();

	const currentlyPressed = useDetectClick(
		elemRef,
		(elem, e) => !!handleEdgeDetection(elem, e)
	);

	const result = useDragging(elemRef, currentlyPressed, (offsetX, offsetY) => {
		if (!elemRef.value) return;

		const rect = elemRef.value.getBoundingClientRect();

		const newSize = {
			w: rect.width,
			h: rect.height,
		};
		const newPos = {
			x: elemRef.value.offsetLeft,
			y: elemRef.value.offsetTop,
		};

		const edge = activeEdge.value;
		if (edge === "right") newSize.w += offsetX;
		else if (edge === "bottom") newSize.h += offsetY;
		else if (edge === "left") {
			newSize.w -= offsetX;
			newPos.x += offsetX;
			if (newPos.x < elemRef.value.offsetLeft + newSize.w - 60) {
				movingToPos.value = newPos;
			}
		} else if (edge === "top") {
			newSize.h -= offsetY;
			newPos.y += offsetY;
			if (newPos.y < elemRef.value.offsetTop + newSize.h - 60) {
				movingToPos.value = newPos;
			}
		}

		const newSizeFinal = {
			w: newSize.w < 60 ? 60 : newSize.w,
			h: newSize.h < 60 ? 60 : newSize.h,
		};
		movingToSize.value = newSizeFinal;

		return {
			...newSizeFinal,
			...newPos,
		};
	});

	watch(initialSize, () => {
		movingToSize.value = initialSize.value;
	});

	watch(movingToSize, () => {
		if (!movingToSize.value || !elemRef.value) return;

		elemRef.value.style.width = `${movingToSize.value.w}px`;
		elemRef.value.style.height = `${movingToSize.value.h}px`;
	});

	watch(movingToPos, () => {
		if (!movingToPos.value || !elemRef.value) return;

		elemRef.value.style.top = `${movingToPos.value.y}px`;
		elemRef.value.style.left = `${movingToPos.value.x}px`;
	});

	watch(activeEdge, () => {
		if (!elemRef.value) return;

		switch (activeEdge.value) {
			case "top":
				return (elemRef.value.style.cursor = "n-resize");
			case "bottom":
				return (elemRef.value.style.cursor = "s-resize");
			case "left":
				return (elemRef.value.style.cursor = "w-resize");
			case "right":
				return (elemRef.value.style.cursor = "e-resize");
			case undefined:
				return (elemRef.value.style.cursor = "");
		}
	});

	const mousemoveCb = (e: MouseEvent) => {
		if (!elemRef.value) return;
		const rect = elemRef.value.getBoundingClientRect();
		activeEdge.value = handleEdgeDetection(rect, e);
	};

	onMounted(() => {
		window.addEventListener("mousemove", mousemoveCb);
	});

	onUnmounted(() => {
		window.removeEventListener("mousemove", mousemoveCb);
	});

	return result;
}
