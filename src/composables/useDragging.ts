import { Ref, onMounted, onUnmounted, ref } from "vue";
import { Pos } from "./useMovable";

export function useDragging<T>(
	elemRef: Ref<HTMLElement | undefined>,
	currentlyPressed: Ref<boolean>,
	onmousemoveExtend: (offsetX: number, offsetY: number, event: MouseEvent) => T
): Ref<T | undefined> {
	const draggingFrom = ref<Pos>();
	const wasDragged = ref(false);

	const readyToResult = ref<T>();
	const result = ref<T>();

	const mousedownCb = (e: MouseEvent) => {
		draggingFrom.value = {
			x: e.clientX,
			y: e.clientY,
		};
	};

	const mousemoveCb = (e: MouseEvent) => {
		if (!elemRef.value) return;

		if (
			currentlyPressed.value &&
			draggingFrom.value &&
			e.buttons >= 1 &&
			e.button === 0
		) {
			const offsetX = e.clientX - draggingFrom.value.x;
			const offsetY = e.clientY - draggingFrom.value.y;
			readyToResult.value = onmousemoveExtend(offsetX, offsetY, e);

			draggingFrom.value = {
				x: e.clientX,
				y: e.clientY,
			};
			wasDragged.value = true;
		}
	};

	const mouseupCb = () => {
		if (currentlyPressed.value && wasDragged.value) {
			result.value = readyToResult.value;
			wasDragged.value = false;
		}
	};

	onMounted(() => {
		window.addEventListener("mousedown", mousedownCb);
		window.addEventListener("mousemove", mousemoveCb);
		window.addEventListener("mouseup", mouseupCb);
	});

	onUnmounted(() => {
		window.removeEventListener("mousedown", mousedownCb);
		window.removeEventListener("mousemove", mousemoveCb);
		window.removeEventListener("mouseup", mouseupCb);
	});

	return result;
}
