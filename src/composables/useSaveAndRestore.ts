import { Ref, onBeforeMount, onUnmounted, ref, watch } from "vue";

type Save<K extends string> = <D extends { [Key in K]: D[Key] }>(
	data: D
) => IDBRequest<IDBValidKey>;

type Restore = <D>(query: IDBValidKey | IDBKeyRange) => IDBRequest<D>;

type Delete = (query: IDBValidKey | IDBKeyRange) => IDBRequest<undefined>;

export function useSaveAndRestore<K extends string>(
	dbName: string,
	storeName: string,
	keyPath: K
): [
	Ref<Save<K> | undefined>,
	Ref<Restore | undefined>,
	Ref<Delete | undefined>
] {
	const db = ref<IDBDatabase>();

	onBeforeMount(() => {
		const req = window.indexedDB.open(dbName, 1);
		req.addEventListener("error", (e) => {
			console.log("Error opening database:", e.target);
		});
		req.addEventListener("success", (e) => {
			db.value = (e.target as IDBOpenDBRequest).result;
		});
		req.addEventListener("upgradeneeded", async (e) => {
			const database = (e.target as IDBOpenDBRequest).result;
			database.createObjectStore(storeName, { keyPath });
			db.value = database;
		});
	});

	onUnmounted(() => {
		db.value?.close();
	});

	const save = ref<Save<K>>();
	const restore = ref<Restore>();
	const del = ref<Delete>();

	watch(db, () => {
		if (!db.value) return;
		save.value = (data) => {
			if (!db.value) throw new Error("There's no database");
			const transaction = db.value.transaction(storeName, "readwrite");
			const store = transaction.objectStore(storeName);
			return store.put(data);
		};
		restore.value = (query) => {
			if (!db.value) throw new Error("There's no database");
			const transaction = db.value.transaction(storeName, "readonly");
			const store = transaction.objectStore(storeName);
			return store.get(query);
		};
		del.value = (query) => {
			if (!db.value) throw new Error("There's no database");
			const transaction = db.value.transaction(storeName, "readwrite");
			const store = transaction.objectStore(storeName);
			return store.delete(query);
		};
	});

	return [save, restore, del];
}
