import { Ref, computed, ref, toRef, watch, watchEffect } from "vue";
import { useSaveAndRestore } from "./useSaveAndRestore";
import { Pos, useMovable } from "./useMovable";
import { useZStack } from "./useZStack";
import { useDetectClick } from "./useDetectClick";
import { Size, useResizable } from "./useResizable";

type Stored = {
	id: IDBValidKey;
	x?: number;
	y?: number;
	z?: number;
	w?: number;
	h?: number;
	visible?: boolean;
};

export function useWindow(
	elemRef: Ref<HTMLElement | undefined>,
	id: IDBValidKey,
	providedPos: Pos,
	providedSize: Size,
	visibilityTrigger: Ref<boolean>
) {
	const [save, restore] = useSaveAndRestore("windows", "state", "id");
	const restored = ref<Partial<Stored>>();

	const currentVisibility = ref<boolean>();
	const currentNonVisibility = computed(() => !currentVisibility.value);
	const offsetParentRect = computed(() =>
		elemRef.value?.offsetParent?.getBoundingClientRect()
	);

	const lastUpdatedPos = ref<Pos>();

	const initialPos = ref<Pos>();
	const movedTo = useMovable(
		elemRef,
		initialPos,
		(rect, e) => detectBox(rect, e) && !determineEdge(rect, e)
	);

	const initialZIndex = ref<number>();
	const currentlyPressed = useDetectClick(elemRef);
	const toTopRef = toRef(
		() => currentlyPressed.value || currentNonVisibility.value
	);
	const zIndex = useZStack(initialZIndex, toTopRef, Symbol());

	const initialSize = ref<Size>();
	const resized = useResizable(elemRef, initialSize, (rect, e) => {
		if (detectBox(rect, e)) return determineEdge(rect, e);
	});

	watch(movedTo, () => {
		if (movedTo.value) lastUpdatedPos.value = movedTo.value;
	});

	watch(resized, () => {
		if (resized.value) {
			lastUpdatedPos.value = {
				x: resized.value.x,
				y: resized.value.y,
			};
		}
	});

	watch(zIndex, () => {
		if (!zIndex.value || !elemRef.value) return;

		elemRef.value.style.zIndex = `${zIndex.value}`;
	});

	watch(currentVisibility, () => {
		if (!elemRef.value || !offsetParentRect.value) return;
		elemRef.value.style.display = currentVisibility.value ? "" : "none";
		if (!currentVisibility.value && initialSize.value) {
			initialPos.value = {
				x:
					elemRef.value.offsetLeft +
					(offsetParentRect.value.width - initialSize.value.w) / 2,
				y:
					elemRef.value.offsetTop +
					(offsetParentRect.value.height - initialSize.value.h) / 2,
			};
			lastUpdatedPos.value = initialPos.value;
		}
	});

	watch(visibilityTrigger, () => {
		currentVisibility.value = !currentVisibility.value;
	});

	// Late hack
	const dbError = ref();
	watch(dbError, () => {
		if (!dbError.value) return;

		initialPos.value = providedPos;
		initialSize.value = providedSize;
	});

	watch(restore, () => {
		if (!restore.value) return;

		try {
			const req = restore.value(id);
			req.addEventListener("success", (e) => {
				const data: Stored | undefined = (e.target as IDBRequest).result;

				if (!data) restored.value = {};
				else restored.value = data;

				initialPos.value = {
					x: data?.x ?? providedPos.x,
					y: data?.y ?? providedPos.y,
				};
				initialZIndex.value = data?.z;
				initialSize.value = {
					w: data?.w ?? providedSize.w,
					h: data?.h ?? providedSize.h,
				};
				currentVisibility.value = data?.visible ?? true;
			});
			req.addEventListener("error", (e) => {
				console.log("Failed to restore state:", e);
			});
		} catch (e) {
			dbError.value = e;
		}
	});

	watchEffect(() => {
		if (!save.value || !restored.value) return;

		try {
			const req = save.value<Stored>({
				id: id,
				x: lastUpdatedPos.value?.x ?? restored.value.x,
				y: lastUpdatedPos.value?.y ?? restored.value.y,
				z: zIndex.value ?? restored.value.z,
				w: resized.value?.w ?? restored.value.w,
				h: resized.value?.h ?? restored.value.h,
				visible: currentVisibility.value,
			});
			req.addEventListener("error", (e) => {
				console.log("Failed to save state:", e);
			});
		} catch (e) {
			dbError.value = e;
		}
	});
}

function detectBox(rect: DOMRect, e: MouseEvent) {
	return (
		rect.left < e.clientX &&
		rect.right > e.clientX &&
		rect.top < e.clientY &&
		rect.bottom > e.clientY
	);
}

function determineEdge(rect: DOMRect, e: MouseEvent) {
	if (rect.left + 20 > e.clientX) return "left";
	else if (rect.right - 20 < e.clientX) return "right";
	else if (rect.top + 20 > e.clientY) return "top";
	else if (rect.bottom - 20 < e.clientY) return "bottom";
	else return undefined;
}
