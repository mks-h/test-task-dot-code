import { Ref, onBeforeMount, onUnmounted, reactive, ref, watch } from "vue";

const ZStack = reactive<{
	stack: symbol[];
	register: (uid: symbol, index?: number) => number;
	unregister: (uid: symbol) => void;
	index: (uid: symbol) => number;
}>({
	stack: [],
	register(uid, index) {
		const arr = this.stack.filter((v) => v !== uid);

		let result;
		if (index === undefined) {
			const len = arr.push(uid);
			result = len - 1;
		} else {
			arr.splice(index, 0, uid);
			result = index;
		}

		this.stack = arr;
		return result;
	},
	unregister(uid) {
		this.stack = this.stack.filter((v) => v !== uid);
	},
	index(uid) {
		const index = this.stack.findIndex((v) => v === uid);
		if (index === -1) throw new Error("Couldn't find itself in the zStack");
		return index;
	},
});

export function useZStack(
	initialIndex: Ref<number | undefined>,
	toTop: Ref<boolean>,
	uid: symbol
) {
	const zIndex = ref<number>();

	watch(ZStack, () => {
		zIndex.value = ZStack.index(uid);
	});

	watch(initialIndex, () => {
		zIndex.value = initialIndex.value;
	});

	watch(toTop, () => {
		if (toTop.value) {
			const result = ZStack.register(uid);
			zIndex.value = result;
		}
	});

	onBeforeMount(() => {
		ZStack.register(uid);
		zIndex.value = ZStack.index(uid);
	});

	onUnmounted(() => {
		ZStack.unregister(uid);
	});

	return zIndex;
}
