import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import { createRouter, createWebHistory } from "vue-router";
import WelcomeView from "./views/WelcomeView.vue";
import WindowManagerView from "./views/WindowManagerView.vue";
import BitcoinView from "./views/BitcoinView.vue";

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{ path: "/", component: WelcomeView },
		{ path: "/window-manager", component: WindowManagerView },
		{ path: "/crypto", component: BitcoinView },
	],
});

createApp(App).use(router).mount("#app");
